package util

import javafx.application.Platform
import javafx.fxml.FXMLLoader
import javafx.scene.Node
import javafx.scene.layout.AnchorPane
import java.io.IOException
import java.net.URL
import java.util.*

private const val DELAY: Long = 125

fun transportTo(container: AnchorPane, to: URL?) {
    Timer().schedule(
            object : TimerTask() {
                override fun run() {
                    Platform.runLater {
                        try {
                            container.children.setAll(FXMLLoader.load<Any>(to) as Node)
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                }
            }, DELAY)
}