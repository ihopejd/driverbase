package data;

public interface Sources {
    String launch = "/fxml/launch_pane.fxml";
    String signIn = "/fxml/sign_in_pane.fxml";
    String signUp = "/fxml/sign_up_pane.fxml";
    String account = "/fxml/inspector_pane.fxml";
}