package data.database

import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException

object DbConnector {

    private const val DB_DRIVER = "com.mysql.cj.jdbc.Driver"
    private const val UNICODE = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC"
    private const val DB_URL = "jdbc:mysql://localhost:3306/driverbase$UNICODE"
    private const val USERNAME = "root"
    private const val PASSWORD = ""

    fun connection(): Connection? {
        return try {
            Class.forName(DB_DRIVER)
            DriverManager.getConnection(DB_URL, USERNAME, PASSWORD)
        } catch (e: ClassNotFoundException ) {
            e.printStackTrace()
            throw RuntimeException("ClassNotFoundException in connection()")
        } catch (e: SQLException) {
            e.printStackTrace()
            throw RuntimeException("SQLException in connection()")
        }
    }
}