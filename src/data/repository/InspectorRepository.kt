package data.repository

import data.model.Inspector

object InspectorRepository {

    private const val INSPECTOR_LOGIN = "inspector"
    private const val INSPECTOR_PASSWORD = "inspector"

    fun auth(inspector: Inspector): Boolean {
        var valid = false
        if (inspector.login == INSPECTOR_LOGIN && inspector.password == INSPECTOR_PASSWORD) {
            valid = true
        }
        return valid
    }
}