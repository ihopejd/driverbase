package data.model.driver

data class DriverAddress(
        val id: DriverGUID ?= null,
        val registrationAddress: String ?= null,
        val liveAddress: String ?= null
)