package data.model.driver

data class DriverGUID (
        val id: Int ?= null,
        val guid: String ?= null
)