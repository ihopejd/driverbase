package data.model.driver

data class DriverPassport(
        val driverGUID: DriverGUID ?= null,
        val serial: String ?= null,
        val number: String ?= null
)