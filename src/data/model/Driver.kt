package data.model

import data.model.driver.*

data class Driver(
        val info: DriverInfo,
        val passport: DriverPassport,
        val address: DriverAddress,
        val job: DriverJob,
        val contacts: DriverContacts
)