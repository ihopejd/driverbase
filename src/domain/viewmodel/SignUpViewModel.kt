package domain.viewmodel

import data.model.Driver
import data.repository.DriverRepository
import domain.state.FormStateError
import domain.validator.validEmail
import util.DriverParser
import java.util.concurrent.atomic.AtomicBoolean

object SignUpViewModel {

    private lateinit var driver: Driver
    private var formState: FormStateError? = null
    private val valid = AtomicBoolean(false)

    fun setFormState(formStateError: FormStateError) {
        this.formState = formStateError
    }

    fun getDriver(): Driver {
        return this.driver
    }

    fun getFormState(): String? {
        return this.formState?.assignedMessage()
    }

    fun initDriver(guid: String, surname: String, name: String, patronymic: String, passport: String,
                   registrationAddress: String, liveAddress: String, place: String, post: String, phone: String,
                   email: String) {
        this.driver = DriverParser.convertIntoObject(guid, surname, name, patronymic, passport,
                registrationAddress, liveAddress, place, post, phone, email)
    }

    fun validate(): Boolean {
        formState = null
        this.driver.contacts.email?.let {
            validEmail(it) {
                formState = FormStateError.EMAIL_FAIL
                valid.set(false)
            }
        }
        return valid.get()
    }

    fun createDriver(driver: Driver?) {
        DriverRepository.getInstance().createDriver(driver)
    }
}