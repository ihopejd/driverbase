package domain.viewmodel

import data.model.Inspector
import data.repository.InspectorRepository

object SignInViewModel {

    private var badTry: Int = 0

    fun isBadTries(): Boolean {
        return this.badTry == 3
    }

    fun validation(login: String, password: String): Boolean {
        var valid = false
        val inspector = Inspector(login, password)
        if (InspectorRepository.auth(inspector)) {
            valid = true
        }
        return valid
    }

    fun writeBadTry() {
        this.badTry += 1
    }

    fun clearBadTry() {
        this.badTry = 0
    }
}