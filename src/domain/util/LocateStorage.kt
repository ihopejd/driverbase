package domain.util

import java.lang.RuntimeException
import java.net.MalformedURLException
import java.net.URL
import java.util.prefs.Preferences

object LocateStorage {

    private const val LOCATION_KEY = "location"
    private val preferences = Preferences.userNodeForPackage(LocateStorage::class.java)

    fun saveLocation(location: URL) {
        val src = location.toString()
        preferences.put(LOCATION_KEY, src)
    }

    fun getLocation(): URL? {
        val src = preferences[LOCATION_KEY, ""]
        try {
            return URL(src)
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        }
        return null
    }

    fun hasSavedLocation(): Boolean {
        return getLocation() != null
    }
}