package presentation.view;

import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import presentation.pattern.ControllerView;
import util.Root;

public class LaunchView extends ControllerView {

    @FXML private AnchorPane pane;

    @Override
    public void prep() {
        super.setContainer(pane);
    }

    @Override
    public void update() {}

    @FXML
    private void transportToSignIn() {
        super.doTransportTo(Root.INSTANCE.toURL(signIn));
    }

    @FXML
    private void transportToSignUp() {
        super.doTransportTo(Root.INSTANCE.toURL(signUp));
    }
}