package presentation.view;

import domain.viewmodel.SignInViewModel;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import presentation.pattern.ControllerView;
import javafx.util.Duration;
import util.Root;

public class SignInView extends ControllerView {

    @FXML private Button signInButton;
    @FXML private Label errorsLabel;
    @FXML private Label blockLabel;
    @FXML private TextField loginField;
    @FXML private PasswordField passwordField;
    @FXML private AnchorPane pane;
    private Timeline timerLine;

    @Override
    public void prep() {
        super.setContainer(pane);
    }

    @Override
    public void update() {
        Platform.runLater(() -> {
            if (super.lockDown.isEmptyAuthLockdown()) {
                super.lockDown.freeAuthLockdown();
                SignInViewModel.INSTANCE.clearBadTry();
                unLockdownUi();
            }
        });
    }

    @FXML
    private void transportToAccount() {
        if (SignInViewModel.INSTANCE.validation(
                loginField.getText(), passwordField.getText()
        )) {
            super.doTransportTo(Root.INSTANCE.toURL(account));
        } else {
            SignInViewModel.INSTANCE.writeBadTry();
        }

        if (SignInViewModel.INSTANCE.isBadTries()) {
            lockdownUi();
        }
    }

    @FXML
    private void transportToBack() {
        super.doTransportTo(Root.INSTANCE.toURL(launch));
    }

    private void unLockdownUi() {
        this.signInButton.setDisable(false);
        this.errorsLabel.setText("");
        this.blockLabel.setText("");
        this.timerLine.stop();
    }

    private void lockdownUi() {
        this.signInButton.setDisable(true);
        this.errorsLabel.setText("Вы израсходовали все доступные количества попыток");
        lockdownTimer();
    }

    private void lockdownTimer() {
        timerLine = new Timeline(new KeyFrame(Duration.ZERO, e -> {
            super.lockDown.decAuthLockdown();
            this.blockLabel.setText(String.valueOf(super.lockDown.getAuthLockdown()));
        }), new KeyFrame(Duration.seconds(1)));
        timerLine.setCycleCount(Animation.INDEFINITE);
        timerLine.play();
    }
}