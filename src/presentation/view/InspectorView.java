package presentation.view;

import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import presentation.pattern.CloseableControllerView;

public class InspectorView extends CloseableControllerView {

    @FXML private AnchorPane pane;

    @Override
    public void prep() {
        super.setContainer(pane);
    }
}