package presentation.pattern;

public interface CloseableListener {
    boolean rule();
    void close();
}